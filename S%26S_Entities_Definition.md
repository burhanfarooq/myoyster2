#### SuitCase

	SuitCase
	{
    	"user_Id": "UserID",
    	"orginaloutfitId": "outFitId",
    	"createDate": "DateFormat",
    	"OutFit": "outfit Object Copy"
	}
	
> **Developer Note**: 
>>1. It looks like SuitCase is/are collection of Outfits and Closet is collection of item. 
2. outfit object is copied b/c user may change outfit item.


> The overriding design goal for Markdown's
> formatting syntax is to make it as readable 
> as possible. The idea is that a
> Markdown-formatted document should be
> publishable as-is, as plain text, without
> looking like it's been marked up with tags
> or formatting instructions.


##### Configure Plugins. Instructions in following README.md files

* plugins/dropbox/README.md
* plugins/github/README.md
* plugins/googledrive/README.md